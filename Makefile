# **************************************************************************** #
#                                                                              #
#                                                         ::::::::             #
#    ft_ls                                              :+:    :+:             #
#                                                      +:+                     #
#    By: dkroeke <dkroeke@student.codam.nl>           +#+                      #
#        nloomans <nloomans@student.codam.nl>        +#+                       #
#                                                   #+#    #+#                 #
#    License: MIT                                  ########   odam.nl          #
#                                                                              #
# **************************************************************************** #

include definitions.mk

ifneq ($(CFLAGS),)
$(call notice,"cflags: $(CFLAGS)")
endif

NAME=ft_ls tester

all: $(NAME)

# Libaries #--------------------------------------------------------------------

LIBFT_NAME=		ft
LIBFT_DIR=		libft
LIBFT_A=		$(LIBFT_DIR)/lib$(LIBFT_NAME).a
LIBFT_IFLAGS=	-I $(LIBFT_DIR)
LIBFT_LFLAGS=	-L $(LIBFT_DIR) -l$(LIBFT_NAME)

$(LIBFT_A):
	@printf "$(BLUE)MAKE$(RESET)\t%s\n" $(LIBFT_DIR)

	@$(MAKE) -C $(LIBFT_DIR)

FTPRINTF_NAME=		ftprintf
FTPRINTF_DIR=		ft_printf
FTPRINTF_A=			$(FTPRINTF_DIR)/lib$(FTPRINTF_NAME).a
FTPRINTF_IFLAGS=	-I $(FTPRINTF_DIR)
FTPRINTF_LFLAGS=	-L $(FTPRINTF_DIR) -l$(FTPRINTF_NAME)

$(FTPRINTF_A):
	@printf "$(BLUE)MAKE$(RESET)\t%s\n" $(FTPRINTF_DIR)

	@$(MAKE) -C $(FTPRINTF_DIR)

CRITERION_NAME=		criterion
ifeq ($(shell uname),Darwin)
CRITERION_A=		$(HOME)/.brew/lib/lib$(CRITERION_NAME).dylib
CRITERION_IFLAGS=	-I $(HOME)/.brew/include
CRITERION_LFLAGS=	-L $(HOME)/.brew/lib -l$(CRITERION_NAME)

$(CRITERION_A):
	@if ! [ -f $(CRITERION_A) ]												;\
	then	echo "Please install criterion using first using:" >&2			;\
			echo >&2														;\
			echo "    brew install criterion" >&2							;\
			exit 1															;\
	fi
else
CRITERION_LFLAGS=	-l$(CRITERION_NAME)
endif

# Definitions #-----------------------------------------------------------------

SRC_DIR=		src
INC_DIR=		inc
OBJ_DIR=		obj

SRC_FILES=		$(wildcard $(SRC_DIR)/*.c)
BIN_SRC_FILES=	$(filter-out %.spec.c,$(SRC_FILES))
INC_FILES=		$(wildcard $(INC_DIR)/*.h)
OBJ_FILES=		$(patsubst $(SRC_DIR)/%.c,$(OBJ_DIR)/%.o,$(SRC_FILES))
BIN_OBJ_FILES=	$(filter-out %.spec.o,$(OBJ_FILES))
TST_OBJ_FILES=	$(filter-out $(OBJ_DIR)/main.o,$(OBJ_FILES))

CFLAGS=			-Werror -Wall -Wextra
BIN_IFLAGS=		-I $(INC_DIR) $(LIBFT_IFLAGS) $(FTPRINTF_IFLAGS)
TST_IFLAGS=		$(BIN_IFLAGS) $(CRITERION_IFLAGS)
BIN_LFLAGS=		$(LIBFT_LFLAGS) $(FTPRINTF_LFLAGS)
TST_LFLAGS=		$(BIN_LFLAGS) $(CRITERION_LFLAGS)

# Regular rules #---------------------------------------------------------------

ft_ls: $(BIN_OBJ_FILES) $(LIBFT_A) $(FTPRINTF_A)
	@printf "$(GREEN)LINK$(RESET)\t%s\n" $@

	@$(CC) -o $@ $(BIN_OBJ_FILES) $(CFLAGS) $(BIN_LFLAGS)

tester: $(TST_OBJ_FILES) $(LIBFT_A) $(FTPRINTF_A) $(CRITERION_A)
	@printf "$(GREEN)LINK$(RESET)\t%s\n" $@

	@$(CC) -o $@ $(TST_OBJ_FILES) $(CFLAGS) $(TST_LFLAGS)

$(OBJ_DIR):
	@printf "$(GREEN)MKDIR$(RESET)\t%s\n" $@

	@mkdir -p $@

$(OBJ_DIR)/%.spec.o: $(SRC_DIR)/%.spec.c $(INC_FILES) | $(OBJ_DIR)
	@printf "$(GREEN)CC$(RESET)\t%s\n" $@

	@$(CC) -o $@ -c $< $(CFLAGS) $(TST_IFLAGS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c $(INC_FILES) | $(OBJ_DIR)
	@printf "$(GREEN)CC$(RESET)\t%s\n" $@

	@$(CC) -o $@ -c $< $(CFLAGS) $(BIN_IFLAGS)

.clang_complete:
	@printf "$(GREEN)GEN$(RESET)\t%s\n" $@

	@echo -Ilibft > .clang_complete
	@echo -Ift_printf >> .clang_complete
	@echo -Iinc >> .clang_complete
ifeq ($(shell uname),Darwin)
	@echo -I$(HOME)/.brew/include >> .clang_complete
endif

# Phonies #---------------------------------------------------------------------

test: tester
	@printf "$(BLUE)RUN$(RESET)\t%s\n" tester

	@./tester

ifeq ($(shell uname),Darwin)
test-valgrind:
	@printf "$(BLUE)DOCKER$(RESET)\t%s\n" test-valgrind

	@docker-machine start || true
	@eval $$(docker-machine env) && \
		docker run --rm --init -v "$$PWD:/project" nloomans/codam \
		make $(MFLAGS) $(MAKEOVERRIDES) -s -C /project test-valgrind
else
test-valgrind: tester
	@printf "$(BLUE)RUN$(RESET)\t%s\n" tester

	@valgrind --trace-children=yes --quiet \
		--track-origins=yes \
		--leak-check=full --show-leak-kinds=definite \
		./tester --verbose
endif

norm:
	@printf "$(BLUE)RUN$(RESET)\t%s\n" norminette

	@norminette $(INC_FILES) $(BIN_SRC_FILES)

clean:
	@$(MAKE) -C $(LIBFT_DIR) fclean
	@$(MAKE) -C $(FTPRINTF_DIR) fclean
	@rm -rf $(OBJ_DIR)

fclean: clean
	@$(MAKE) -C $(LIBFT_DIR) fclean
	@$(MAKE) -C $(FTPRINTF_DIR) fclean
	@rm -f $(NAME)

re:
	@$(MAKE) fclean
	@$(MAKE)

# Special #---------------------------------------------------------------------

.SECONDARY: $(OBJ_FILES)
.PHONY: all test test-valgrind norm clean fclean re
