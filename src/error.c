/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "global.h"
#include "error.h"

void	recovered_error(char *path)
{
	if (path != NULL)
		ft_dprintf(2, "%s: %s: %s\n", g_argv0, path, strerror(errno));
	else
		ft_dprintf(2, "%s: %s\n", g_argv0, strerror(errno));
	g_exit_code = ERROR_RECOVERED;
}

void	invalid_arg_error(char *arg)
{
	ft_dprintf(2, "%s: illegal option -- %c\n", g_argv0, *arg);
	ft_dprintf(2, "usage: %s [-lRart]\n", g_argv0);
	exit(ERROR_USER);
}
