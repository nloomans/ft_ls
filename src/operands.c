/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <errno.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <libft.h>
#include <ft_printf.h>
#include "fs.h"
#include "operands.h"
#include "global.h"
#include "error.h"
#include "opt.h"

void	resolve_operands(struct s_fs_file **files, char **operands, t_opt opt)
{
	if (*operands == NULL)
	{
		*files = NULL;
		return ;
	}
	*files = (struct s_fs_file *)ft_memalloc(sizeof(**files));
	if (*files == NULL)
	{
		perror(g_argv0);
		exit(ERROR_FATAL);
	}
	if (fs_read_file(*files, *operands, OPT_ALL) == -1)
	{
		recovered_error(*operands);
		ft_memdel((void **)files);
		resolve_operands(files, operands + 1, opt);
	}
	else
		resolve_operands(&(*files)->next, operands + 1, opt);
}

void	split_operands(struct s_fs_file **files, struct s_fs_file **dirs,
			t_opt opt, struct s_fs_file **head)
{
	struct s_fs_file	*operand;

	if (*head == NULL)
		return ;
	operand = *head;
	*head = operand->next;
	operand->next = NULL;
	if (S_ISDIR(operand->mode) ||
		(!(opt & OPT_LONG) &&
			S_ISLNK(operand->mode) && S_ISDIR(operand->lnk_mode)))
	{
		*dirs = operand;
		return (split_operands(files, &(*dirs)->next, opt, head));
	}
	else
	{
		*files = operand;
		return (split_operands(&(*files)->next, dirs, opt, head));
	}
}
