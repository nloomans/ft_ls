/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <libft.h>
#define CR_PIPE_READ 0
#define CR_PIPE_WRITE 1

int			g_read_fd = 0;

void		cr_stdout_init(void)
{
	int				fds[2];

	if (g_read_fd != 0)
		cr_assert_fail(
			"cr_stdout_init may only be called once per Test(...)\n");
	if (pipe(fds) == -1)
		cr_assert_fail("pipe: %s", strerror(errno));
	close(1);
	if (dup2(fds[CR_PIPE_WRITE], 1) == -1)
		cr_assert_fail("dup2: %s", strerror(errno));
	g_read_fd = fds[CR_PIPE_READ];
}

void		cr_stdout_eq(char *expected)
{
	int		amount_read;
	char	buffer[4096 + 1];

	if (g_read_fd == 0)
		cr_assert_fail(
			"cr_stdout_init has not yet been called\n");
	if ((amount_read = read(g_read_fd, buffer, 4096)) == -1)
		cr_assert_fail("read: %s", strerror(errno));
	buffer[amount_read] = 0;
	cr_expect_str_eq(buffer, expected);
	close(g_read_fd);
}
