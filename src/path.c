/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include <libft.h>
#include "path.h"

char	*get_pathname(const char *path, const char *name)
{
	size_t	pathlen;
	size_t	namelen;
	char	*dest;

	pathlen = ft_strlen(path);
	namelen = ft_strlen(name);
	if (ft_strcmp(path, "/") == 0)
		pathlen = 0;
	dest = ft_strnew(pathlen + 1 + namelen);
	if (dest == NULL)
		return (NULL);
	ft_strncpy(dest, path, pathlen);
	dest[pathlen] = '/';
	ft_strncpy(dest + pathlen + 1, name, namelen);
	dest[pathlen + 1 + namelen] = '\0';
	return (dest);
}

char	*get_basename(const char *pathname)
{
	char *base;

	base = ft_strrchr(pathname, '/');
	return ((char *)(base ? base + 1 : pathname));
}
