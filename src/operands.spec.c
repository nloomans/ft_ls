/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "operands.h"

Test(split_operands, regular_file) {
	struct s_fs_file *operands = &(struct s_fs_file){
		.pathname = "README.md",
		.mode = 0100644,
		.lnk_mode = 0,
		.next = NULL
	};

	struct s_fs_file *files = NULL;
	struct s_fs_file *dirs = NULL;

	split_operands(&files, &dirs, 0, &operands);
	cr_expect_eq(dirs, NULL);
	cr_expect_str_eq(files->pathname, "README.md");
	cr_expect_eq(files->mode, 0100644);
	cr_expect_eq(files->next, NULL);
	cr_expect_eq(operands, NULL);
}

Test(split_operands, directory) {
	struct s_fs_file *operands = &(struct s_fs_file){
		.pathname = "src",
		.mode = 040755,
		.lnk_mode = 0,
		.next = NULL
	};

	struct s_fs_file *files = NULL;
	struct s_fs_file *dirs = NULL;

	split_operands(&files, &dirs, 0, &operands);
	cr_expect_eq(files, NULL);
	cr_expect_str_eq(dirs->pathname, "src");
	cr_expect_eq(dirs->mode, 040755);
	cr_expect_eq(dirs->next, NULL);
	cr_expect_eq(operands, NULL);
}

Test(split_operands, link_to_file) {
	struct s_fs_file *operands = &(struct s_fs_file){
		.pathname = "~/.zshrc",
		.mode = 0120755,
		.lnk_mode = 0100600,
		.next = NULL
	};

	struct s_fs_file *files = NULL;
	struct s_fs_file *dirs = NULL;

	split_operands(&files, &dirs, 0, &operands);
	cr_expect_eq(dirs, NULL);
	cr_expect_str_eq(files->pathname, "~/.zshrc");
	cr_expect_eq(files->mode, 0120755);
	cr_expect_eq(files->lnk_mode, 0100600);
	cr_expect_eq(files->next, NULL);
	cr_expect_eq(operands, NULL);
}

Test(split_operands, link_to_dir_short) {
	struct s_fs_file *operands = &(struct s_fs_file){
		.pathname = "goinfre",
		.mode = 0120755,
		.lnk_mode = 040700,
		.next = NULL
	};

	struct s_fs_file *files = NULL;
	struct s_fs_file *dirs = NULL;

	split_operands(&files, &dirs, 0, &operands);
	cr_expect_eq(files, NULL);
	cr_expect_str_eq(dirs->pathname, "goinfre");
	cr_expect_eq(dirs->mode, 0120755);
	cr_expect_eq(dirs->lnk_mode, 040700);
	cr_expect_eq(dirs->next, NULL);
	cr_expect_eq(operands, NULL);
}

Test(split_operands, link_to_dir_long) {
	struct s_fs_file *operands = &(struct s_fs_file){
		.pathname = "goinfre",
		.mode = 0120755,
		.lnk_mode = 040700,
		.next = NULL
	};

	struct s_fs_file *files = NULL;
	struct s_fs_file *dirs = NULL;

	split_operands(&files, &dirs, OPT_LONG, &operands);
	cr_expect_eq(dirs, NULL);
	cr_expect_str_eq(files->pathname, "goinfre");
	cr_expect_eq(files->mode, 0120755);
	cr_expect_eq(files->lnk_mode, 040700);
	cr_expect_eq(files->next, NULL);
	cr_expect_eq(operands, NULL);
}

Test(split_operands, file_and_dir) {
	struct s_fs_file *operands = &(struct s_fs_file){
		.pathname = "README.md",
		.mode = 0100644,
		.lnk_mode = 0,
		// I need to initialize lnk_pathname if .next is defined, otherwise GCC
		// will complain. Not sure why.
		.lnk_pathname = "",
		.next = &(struct s_fs_file){
			.pathname = "src",
			.mode = 040755,
			.lnk_mode = 0,
			.next = NULL
		}
	};

	struct s_fs_file *files = NULL;
	struct s_fs_file *dirs = NULL;

	split_operands(&files, &dirs, 0, &operands);
	cr_expect_str_eq(dirs->pathname, "src");
	cr_expect_eq(dirs->mode, 040755);
	cr_expect_eq(dirs->next, NULL);

	cr_expect_str_eq(files->pathname, "README.md");
	cr_expect_eq(files->mode, 0100644);
	cr_expect_eq(files->next, NULL);

	cr_expect_eq(operands, NULL);
}
