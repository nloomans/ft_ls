/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifdef __linux__
# include <sys/sysmacros.h>
#else
# include <sys/types.h>
#endif
#include <sys/stat.h>
#include <stdlib.h>
#include "path.h"
#include "fs.h"
#include "long_display.h"
#include "ft_printf.h"
#include "libft.h"
#include "conversions.h"
#include "opt.h"

void		major_minor_size(mode_t mode, dev_t rdev, struct s_width *widths)
{
	int				maj;
	int				min;

	if (S_ISBLK(mode) || S_ISCHR(mode))
	{
		maj = major(rdev);
		min = minor(rdev);
		if ((size_t)ft_intlen(maj) > widths->major)
			widths->major = ft_intlen(maj);
		if ((size_t)ft_intlen(min) > widths->minor)
			widths->minor = ft_intlen(min);
		if (widths->major + widths->minor + 2 > widths->size)
			widths->size = widths->major + widths->minor + 2;
	}
}

void		size_width(struct s_width *widths, struct s_fs_file *files)
{
	size_t		length;

	if (files == NULL)
		return ;
	length = ft_intlen(files->size);
	if (length > widths->size)
		widths->size = length;
	major_minor_size(files->mode, files->rdev, widths);
	length = ft_intlen(files->nlink);
	if (length > widths->link)
		widths->link = length;
	length = ft_strlen(files->uid_name);
	if (length > widths->uid)
		widths->uid = length;
	length = ft_strlen(files->gid_name);
	if (length > widths->gid)
		widths->gid = length;
	return (size_width(widths, files->next));
}

void		print_long_display(struct s_fs_file *files, int standalone,
								struct s_width *widths, t_opt opt)
{
	t_modestr			permission;
	char				time[64];

	if (files == NULL)
		return ;
	mode_conversion(permission, files->mode);
	time_conversion(time, files, opt);
	ft_printf("%s %*ld %-*s ", permission, widths->link, files->nlink,
		widths->uid, files->uid_name);
	if (!(opt & OPT_OMIT_GROUP))
		ft_printf("%-*s ", widths->gid, files->gid_name);
	if (S_ISBLK(files->mode) || S_ISCHR(files->mode))
		ft_printf("%*d, %*d ", widths->major, major(files->rdev),
			widths->minor, minor(files->rdev));
	else
		ft_printf("%*ld ", widths->size, files->size);
	ft_printf("%s ", time);
	if (standalone)
		ft_printf("%s", files->pathname);
	else
		ft_printf("%s", get_basename(files->pathname));
	if (S_ISLNK(files->mode))
		ft_printf(" -> %s", files->lnk_pathname);
	ft_putchar('\n');
	return (print_long_display(files->next, standalone, widths, opt));
}

int			total_blocks(struct s_fs_file *files, int count)
{
	if (files == NULL)
		return (count);
	count += (int)files->blocks;
	return (total_blocks(files->next, count));
}

void		long_display(struct s_fs_file *files, int standalone, t_opt opt)
{
	struct s_width	widths;

	ft_bzero(&widths, sizeof(struct s_width));
	size_width(&widths, files);
	if (!standalone)
		ft_printf("total %d\n", total_blocks(files, 0));
	print_long_display(files, standalone, &widths, opt);
}
