/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "fs.h"
#include "ft_printf.h"
#include "sort.h"

struct s_fs_file	*fs_reverse(struct s_fs_file *head)
{
	struct s_fs_file *tail;

	if (head == NULL || head->next == NULL)
		return (head);
	tail = fs_reverse(head->next);
	head->next->next = head;
	head->next = NULL;
	return (tail);
}

void				split_list(struct s_fs_file **front,
						struct s_fs_file **back, struct s_fs_file *list)
{
	struct s_fs_file	*fast;
	struct s_fs_file	*slow;

	slow = list;
	fast = list->next;
	while (fast != NULL)
	{
		fast = fast->next;
		if (fast != NULL)
		{
			slow = slow->next;
			fast = fast->next;
		}
	}
	*front = list;
	*back = slow->next;
	slow->next = NULL;
}

struct s_fs_file	*function_merge(struct s_fs_file *a, struct s_fs_file *b,
					int sort_function(struct s_fs_file*, struct s_fs_file*))
{
	struct s_fs_file *result;

	result = NULL;
	if (a == NULL)
		return (b);
	else if (b == NULL)
		return (a);
	if (sort_function(a, b) < 0)
	{
		result = a;
		result->next = function_merge(a->next, b, sort_function);
	}
	else
	{
		result = b;
		result->next = function_merge(a, b->next, sort_function);
	}
	return (result);
}

void				merge_sort_list(struct s_fs_file **files,
						int sort_function(struct s_fs_file*, struct s_fs_file*))
{
	struct s_fs_file	*head;
	struct s_fs_file	*a;
	struct s_fs_file	*b;

	head = *files;
	if (head == NULL || head->next == NULL)
		return ;
	a = NULL;
	b = NULL;
	split_list(&a, &b, *files);
	merge_sort_list(&a, sort_function);
	merge_sort_list(&b, sort_function);
	*files = function_merge(a, b, sort_function);
}
