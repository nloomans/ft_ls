/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <ft_printf.h>
#include <libft.h>
#include "global.h"
#include "array.h"
#include "error.h"
#include "opt.h"

static int			handle_opt(t_opt *opt, char arg)
{
	if (arg == 'l')
		*opt |= OPT_LONG;
	else if (arg == 'R')
		*opt |= OPT_RECURSIVE;
	else if (arg == 'u')
		*opt |= OPT_SORT_ATIME;
	else if (arg == 'o')
		*opt |= OPT_OMIT_GROUP | OPT_LONG;
	else if (arg == 'n')
		*opt |= OPT_ID_NUM | OPT_LONG;
	else if (arg == 'a')
		*opt |= OPT_ALL;
	else if (arg == 'T')
		*opt |= OPT_LONG_TIME;
	else if (arg == 'r')
		*opt |= OPT_REVERSE;
	else if (arg == 't')
		*opt = (*opt & ~OPT_SORT_SIZE) | OPT_SORT_TIME;
	else if (arg == 'S')
		*opt = (*opt & ~OPT_SORT_TIME) | OPT_SORT_SIZE;
	else
		return (-1);
	return (0);
}

static void			handle_arg(t_opt *opt, char *arg)
{
	while (*arg != '\0')
	{
		if (handle_opt(opt, *arg) == -1)
			invalid_arg_error(arg);
		arg++;
	}
}

t_opt				parse_opt(int argc, char **argv)
{
	int		i;
	t_opt	opt;

	i = 0;
	opt = 0;
	while (i < argc)
	{
		if (argv[i][0] == '-')
			handle_arg(&opt, argv[i] + 1);
		i++;
	}
	return (opt);
}

struct s_str_array	parse_operands(int argc, char **argv)
{
	int					i;
	struct s_str_array	operands;

	i = 0;
	operands.len = 0;
	operands.array = (char **)ft_memalloc((argc + 1) * sizeof(char *));
	if (operands.array == NULL)
	{
		perror(g_argv0);
		exit(ERROR_FATAL);
	}
	while (i < argc)
	{
		if (argv[i][0] != '-' && argv[i][0] != '\0')
		{
			operands.array[operands.len] = argv[i];
			operands.len++;
		}
		i++;
	}
	return (operands);
}
