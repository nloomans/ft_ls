/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <libft.h>
#include <sys/stat.h>
#include "filter.h"

int		filter_out_none(struct s_fs_file *file)
{
	(void)file;
	return (1);
}


Test(filter, filters_out_none) {
	struct s_fs_file *head = ft_memalloc(sizeof(struct s_fs_file));
	head->next = ft_memalloc(sizeof(struct s_fs_file));
	head->next->next = ft_memalloc(sizeof(struct s_fs_file));

	filter(&head, filter_out_none);

	cr_assert_neq(head, NULL);
	cr_assert_neq(head->next, NULL);
	cr_assert_neq(head->next->next, NULL);
	cr_assert_eq(head->next->next->next, NULL);

	fs_free_files(&head);
}

int		filter_out_all(struct s_fs_file *file)
{
	(void)file;
	return (0);
}

Test(filter, filters_out_all) {
	struct s_fs_file *head = ft_memalloc(sizeof(struct s_fs_file));
	head->next = ft_memalloc(sizeof(struct s_fs_file));
	head->next->next = ft_memalloc(sizeof(struct s_fs_file));

	filter(&head, filter_out_all);

	cr_assert_eq(head, NULL);

	fs_free_files(&head);
}

int		filter_out_size_42(struct s_fs_file *file)
{
	return (file->size != 42);
}

Test(filter, filters_out_start) {
	struct s_fs_file *head = ft_memalloc(sizeof(struct s_fs_file));
	head->next = ft_memalloc(sizeof(struct s_fs_file));
	head->next->next = ft_memalloc(sizeof(struct s_fs_file));
	head->size = 42;

	filter(&head, filter_out_size_42);

	cr_assert_neq(head, NULL);
	cr_assert_neq(head->next, NULL);
	cr_assert_eq(head->next->next, NULL);

	fs_free_files(&head);
}

Test(filter, filters_out_end) {
	struct s_fs_file *head = ft_memalloc(sizeof(struct s_fs_file));
	head->next = ft_memalloc(sizeof(struct s_fs_file));
	head->next->next = ft_memalloc(sizeof(struct s_fs_file));
	head->next->next->size = 42;

	filter(&head, filter_out_size_42);

	cr_assert_neq(head, NULL);
	cr_assert_neq(head->next, NULL);
	cr_assert_eq(head->next->next, NULL);

	fs_free_files(&head);
}

Test(filter_out_hidden_files, normal) {
	struct s_fs_file hidden_file = {
		.pathname = ".foo",
	};
	cr_expect_eq(filter_out_hidden_files(&hidden_file), 0);

	struct s_fs_file normal_file = {
		.pathname = "foo",
	};
	cr_expect_eq(filter_out_hidden_files(&normal_file), 1);
}

Test(filter_out_hidden_files, within_dir) {
	struct s_fs_file hidden_file = {
		.pathname = "dir/.foo",
	};
	cr_expect_eq(filter_out_hidden_files(&hidden_file), 0);

	struct s_fs_file normal_file = {
		.pathname = "dir/foo",
	};
	cr_expect_eq(filter_out_hidden_files(&normal_file), 1);
}

Test(filter_directories, normal) {
	struct s_fs_file dir_file = {
		.mode = S_IFDIR,
	};
	cr_expect_eq(filter_directories(&dir_file), 1);

	struct s_fs_file regular_file = {
		.mode = S_IFREG,
	};
	cr_expect_eq(filter_directories(&regular_file), 0);
}

Test(filter_directories, filers_out_dot_and_dot_dot) {
	struct s_fs_file dot = {
		.pathname = ".",
		.mode = S_IFDIR,
	};
	cr_expect_eq(filter_directories(&dot), 0);

	struct s_fs_file dot_dot = {
		.pathname = "..",
		.mode = S_IFDIR,
	};
	cr_expect_eq(filter_directories(&dot_dot), 0);

	struct s_fs_file dot_foo = {
		.pathname = ".foo",
		.mode = S_IFDIR,
	};
	cr_expect_eq(filter_directories(&dot_foo), 1);
}

Test(filter_directories, filers_out_dot_and_dot_dot_within_dir) {
	struct s_fs_file dot = {
		.pathname = "dir/.",
		.mode = S_IFDIR,
	};
	cr_expect_eq(filter_directories(&dot), 0);

	struct s_fs_file dot_dot = {
		.pathname = "dir/..",
		.mode = S_IFDIR,
	};
	cr_expect_eq(filter_directories(&dot_dot), 0);

	struct s_fs_file dot_foo = {
		.pathname = "dir/.foo",
		.mode = S_IFDIR,
	};
	cr_expect_eq(filter_directories(&dot_foo), 1);
}
