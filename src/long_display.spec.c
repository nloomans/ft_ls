/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ft_printf.h>
#include <time.h>
#include "long_display.h"
#include "libft.h"
#include "fs.h"
#include "cr_stdout.spec.h"
#include "populate.h"

struct s_fs_file file = (struct s_fs_file){
	.pathname = "hello/there",
	.mode = 0060777,
	.gid_name = "bocal",
	.uid_name = "root",
	.size = 300,
	.nlink = 60,
	.blocks = 30,
	.lnk_pathname = "",
	.mtime = 1566737343,
	.next = NULL,
};

Test(long_display, normal) {
	setenv("TZ", "UTC0", 1);
	tzset();
	cr_stdout_init();
	long_display(&file, 0, OPT_LONG);
	cr_stdout_eq("total 30\nbrwxrwxrwx 60 root bocal 0, 0 Aug 25 12:49 there\n");
}

Test(long_display, standalone) {
	setenv("TZ", "UTC0", 1);
	tzset();
	cr_stdout_init();
	long_display(&file, 1, OPT_LONG);
	cr_stdout_eq("brwxrwxrwx 60 root bocal 0, 0 Aug 25 12:49 hello/there\n");
}

Test(long_display, multiple_files) {
	struct s_fs_file 	*test;

	test = malloc(sizeof(struct s_fs_file));
	ft_strcpy(test->pathname, "This");
	test->mode = 0010447;
	ft_strcpy(test->gid_name, "root");
	ft_strcpy(test->uid_name, "root");
	test->size = 300;
	test->nlink = 100;
	ft_strcpy(test->lnk_pathname, "hello");
	test->mtime = 1566737343;
	test->blocks = 10;

	test->next = malloc(sizeof(struct s_fs_file));
	ft_strcpy(test->next->pathname, "that");
	test->next->mode = 0010227;
	ft_strcpy(test->next->gid_name, "root");
	ft_strcpy(test->next->uid_name, "root");
	test->next->size = 22;
	ft_strcpy(test->next->lnk_pathname, "world");
	test->next->blocks = 20;
	test->next->nlink = 90;
	test->next->mtime = 1566737343;
	test->next->next = NULL;

	setenv("TZ", "UTC0", 1);
	tzset();
	cr_stdout_init();
	long_display(test, 0, OPT_LONG);
	cr_stdout_eq("total 30\npr--r--rwx 100 root root 300 Aug 25 12:49 This\np-w--w-rwx  90 root root  22 Aug 25 12:49 that\n");
	free(test->next);
	free(test);
}

Test(long_display, long_time) {
	struct s_fs_file 	*test;

	test = malloc(sizeof(struct s_fs_file));
	ft_strcpy(test->pathname, "This");
	test->mode = 0010447;
	ft_strcpy(test->gid_name, "root");
	ft_strcpy(test->uid_name, "root");
	test->size = 300;
	test->nlink = 100;
	ft_strcpy(test->lnk_pathname, "hello");
	test->mtime = 1566737343;
	test->blocks = 10;

	test->next = malloc(sizeof(struct s_fs_file));
	ft_strcpy(test->next->pathname, "that");
	test->next->mode = 0010227;
	ft_strcpy(test->next->gid_name, "root");
	ft_strcpy(test->next->uid_name, "root");
	test->next->size = 22;
	ft_strcpy(test->next->lnk_pathname, "world");
	test->next->blocks = 20;
	test->next->nlink = 90;
	test->next->mtime = 1566737343;
	test->next->next = NULL;

	setenv("TZ", "UTC0", 1);
	tzset();
	cr_stdout_init();
	long_display(test, 0, OPT_LONG_TIME);
	cr_stdout_eq("total 30\npr--r--rwx 100 root root 300 Aug 25 12:49:03 2019 This\np-w--w-rwx  90 root root  22 Aug 25 12:49:03 2019 that\n");
	free(test->next);
	free(test);
}

Test(long_display, omit_group) {
	struct s_fs_file 	*test;

	test = malloc(sizeof(struct s_fs_file));
	ft_strcpy(test->pathname, "This");
	test->mode = 0010447;
	ft_strcpy(test->gid_name, "root");
	ft_strcpy(test->uid_name, "root");
	test->size = 300;
	test->nlink = 100;
	ft_strcpy(test->lnk_pathname, "hello");
	test->mtime = 1566737343;
	test->blocks = 10;

	test->next = malloc(sizeof(struct s_fs_file));
	ft_strcpy(test->next->pathname, "that");
	test->next->mode = 0010227;
	ft_strcpy(test->next->gid_name, "root");
	ft_strcpy(test->next->uid_name, "root");
	test->next->size = 22;
	ft_strcpy(test->next->lnk_pathname, "world");
	test->next->blocks = 20;
	test->next->nlink = 90;
	test->next->mtime = 1566737343;
	test->next->next = NULL;

	setenv("TZ", "UTC0", 1);
	tzset();
	cr_stdout_init();
	long_display(test, 0, OPT_OMIT_GROUP);
	cr_stdout_eq("total 30\npr--r--rwx 100 root 300 Aug 25 12:49 This\np-w--w-rwx  90 root  22 Aug 25 12:49 that\n");
	free(test->next);
	free(test);
}
