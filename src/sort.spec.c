/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <libft.h>
#include "fs.h"
#include "sort.h"

Test(fs_reverse, reverses)
{
	struct s_fs_file *head = ft_memalloc(sizeof(struct s_fs_file));
	ft_strncpy(head->pathname, "first", sizeof(head->pathname));
	head->next = ft_memalloc(sizeof(struct s_fs_file));
	ft_strncpy(head->next->pathname, "second", sizeof(head->pathname));
	head->next->next = ft_memalloc(sizeof(struct s_fs_file));
	ft_strncpy(head->next->next->pathname, "third", sizeof(head->pathname));

	head = fs_reverse(head);

	cr_expect_str_eq(head->pathname, "third");
	cr_expect_str_eq(head->next->pathname, "second");
	cr_expect_str_eq(head->next->next->pathname, "first");
	cr_expect_eq(head->next->next->next, NULL);

	fs_free_files(&head);
}

Test(fs_reverse, reverses_empty_list)
{
	struct s_fs_file *head = NULL;

	head = fs_reverse(head);

	cr_expect_eq(head, NULL);
}

Test(fs_reverse, reverses_single_file)
{
	struct s_fs_file *head = ft_memalloc(sizeof(struct s_fs_file));
	ft_strncpy(head->pathname, "one and only", sizeof(head->pathname));

	head = fs_reverse(head);
	cr_expect_str_eq(head->pathname, "one and only");
	cr_expect_eq(head->next, NULL);

	fs_free_files(&head);
}

Test(mergesort, sort_mtime)
{
	struct s_fs_file *head = ft_memalloc(sizeof(struct s_fs_file));
	ft_strncpy(head->pathname, "first", sizeof(head->pathname));
	head->mtime = 10;
	head->next = ft_memalloc(sizeof(struct s_fs_file));
	ft_strncpy(head->next->pathname, "second", sizeof(head->pathname));
	head->next->mtime = 20;
	head->next->next = ft_memalloc(sizeof(struct s_fs_file));
	ft_strncpy(head->next->next->pathname, "third", sizeof(head->pathname));
	head->next->next->mtime = 30;

	merge_sort_list(&head, sort_mtime);
	cr_expect_str_eq("third", head->pathname);
	cr_expect_str_eq("second", head->next->pathname);
	cr_expect_str_eq("first", head->next->next->pathname);
	fs_free_files(&head);
}

Test(mergesort, sort_name)
{
	struct s_fs_file *head = ft_memalloc(sizeof(struct s_fs_file));
	ft_strncpy(head->pathname, "ae", sizeof(head->pathname));
	head->next = ft_memalloc(sizeof(struct s_fs_file));
	ft_strncpy(head->next->pathname, "ad", sizeof(head->pathname));
	head->next->next = ft_memalloc(sizeof(struct s_fs_file));
	ft_strncpy(head->next->next->pathname, "ac", sizeof(head->pathname));
	head->next->next->next = ft_memalloc(sizeof(struct s_fs_file));
	ft_strncpy(head->next->next->next->pathname, "ab", sizeof(head->pathname));
	head->next->next->next->next = ft_memalloc(sizeof(struct s_fs_file));
	ft_strncpy(head->next->next->next->next->pathname, "aa", sizeof(head->pathname));

	merge_sort_list(&head, sort_name);
	cr_expect_str_eq("aa", head->pathname);
	cr_expect_str_eq("ab", head->next->pathname);
	cr_expect_str_eq("ac", head->next->next->pathname);
	cr_expect_str_eq("ad", head->next->next->next->pathname);
	cr_expect_str_eq("ae", head->next->next->next->next->pathname);
	fs_free_files(&head);
}

Test(mergesort, sort_size)
{
	struct s_fs_file *head = ft_memalloc(sizeof(struct s_fs_file));
	ft_strncpy(head->pathname, "aa", sizeof(head->pathname));
	head->size = 800;
	head->next = ft_memalloc(sizeof(struct s_fs_file));
	ft_strncpy(head->next->pathname, "ab", sizeof(head->pathname));
	head->next->size = 488;
	head->next->next = ft_memalloc(sizeof(struct s_fs_file));
	ft_strncpy(head->next->next->pathname, "ac", sizeof(head->pathname));
	head->next->next->size = 633;
	head->next->next->next = ft_memalloc(sizeof(struct s_fs_file));
	ft_strncpy(head->next->next->next->pathname, "ad", sizeof(head->pathname));
	head->next->next->next->size = 783;
	head->next->next->next->next = ft_memalloc(sizeof(struct s_fs_file));
	ft_strncpy(head->next->next->next->next->pathname, "ae", sizeof(head->pathname));
	head->next->next->next->next->size = 1;

	merge_sort_list(&head, sort_size);
	cr_expect_str_eq("aa", head->pathname);
	cr_expect_str_eq("ad", head->next->pathname);
	cr_expect_str_eq("ac", head->next->next->pathname);
	cr_expect_str_eq("ab", head->next->next->next->pathname);
	cr_expect_str_eq("ae", head->next->next->next->next->pathname);
	fs_free_files(&head);
}
