/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   sort_functions.c                                   :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/09/26 10:26:26 by dkroeke        #+#    #+#                */
/*   Updated: 2019/09/26 10:26:26 by dkroeke       ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include "fs.h"
#include "sort.h"
#include "opt.h"
#include "ft_printf.h"

t_sort_func		*get_sort_func(t_opt options)
{
	if (options & OPT_SORT_SIZE)
		return (sort_size);
	if ((options & OPT_SORT_ATIME && options & OPT_SORT_TIME))
		return (sort_atime);
	if (options & OPT_SORT_TIME)
		return (sort_mtime);
	return (sort_name);
}

int				sort_atime(struct s_fs_file *a, struct s_fs_file *b)
{
	return (a->atime == b->atime ? sort_name(a, b) : b->atime - a->atime);
}

int				sort_mtime(struct s_fs_file *a, struct s_fs_file *b)
{
	return (a->mtime == b->mtime ? sort_name(a, b) : b->mtime - a->mtime);
}

int				sort_name(struct s_fs_file *a, struct s_fs_file *b)
{
	return (ft_strcmp(a->pathname, b->pathname));
}

int				sort_size(struct s_fs_file *a, struct s_fs_file *b)
{
	return (b->size - a->size);
}
