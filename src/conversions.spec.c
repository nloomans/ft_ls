/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "criterion/criterion.h"
#include "fs.h"
#include "conversions.h"
#include "libft.h"
#include <time.h>

Test(conversions, normal) {
	struct s_fs_file *test;
	t_modestr	perm;

	test = (struct s_fs_file *)ft_memalloc(sizeof(*test));
	test->mode = 00101644;
	mode_conversion(perm, test->mode);
	cr_expect_str_eq(perm, "-rw-r--r-T");
	test->mode = 00101333;
	mode_conversion(perm, test->mode);
	cr_expect_str_eq(perm, "--wx-wx-wt");
	test->mode = 00100775;
	mode_conversion(perm, test->mode);
	cr_expect_str_eq(perm, "-rwxrwxr-x");
	test->mode = 00100777;
	mode_conversion(perm, test->mode);
	cr_expect_str_eq(perm, "-rwxrwxrwx");
	test->mode = 00040755;
	mode_conversion(perm, test->mode);
	cr_expect_str_eq(perm, "drwxr-xr-x");
	test->mode = 00120777;
	mode_conversion(perm, test->mode);
	cr_expect_str_eq(perm, "lrwxrwxrwx");
	test->mode = 00020644;
	mode_conversion(perm, test->mode);
	cr_expect_str_eq(perm, "crw-r--r--");
	test->mode = 00061777;
	mode_conversion(perm, test->mode);
	cr_expect_str_eq(perm, "brwxrwxrwt");
	test->mode = 00106222;
	mode_conversion(perm, test->mode);
	cr_expect_str_eq(perm, "--wS-wS-w-");
	test->mode = 00106333;
	mode_conversion(perm, test->mode);
	cr_expect_str_eq(perm, "--ws-ws-wx");
	test->mode = 00000000;
	mode_conversion(perm, test->mode);
	cr_expect_str_eq(perm, "----------");
	free(test);
}

Test(time, normal) {
	struct	tm			*timestruct;
	char				postime[64];
	char				time[64];
	struct s_fs_file 	*test;

	test = (struct s_fs_file *)ft_memalloc(sizeof(*test));
	ft_bzero(time, 64);
	test->mtime = 1566737343;
	timestruct = localtime(&test->mtime);
	strftime(postime, 64, "%b %d %H:%M", timestruct);
	time_conversion(time, test, OPT_ALL);
	cr_expect_str_eq(time, postime);

	ft_bzero(time, 64);
	test->mtime = test->mtime - 15778463;
	timestruct = localtime(&test->mtime);
	strftime(postime, 64, "%b %d  %Y", timestruct);
	time_conversion(time, test, OPT_ALL);
	cr_expect_str_eq(time, postime);

	ft_bzero(time, 64);
	test->mtime = test->mtime + (15778463/2);
	timestruct = localtime(&test->mtime);
	strftime(postime, 64, "%b %d %H:%M", timestruct);
	time_conversion(time, test, OPT_ALL);
	cr_expect_str_eq(time, postime);

	ft_bzero(time, 64);
	test->mtime = test->mtime + 15778463;
	timestruct = localtime(&test->mtime);
	strftime(postime, 64, "%b %d  %Y", timestruct);
	time_conversion(time, test, OPT_ALL);
	cr_expect_str_eq(time, postime);

	fs_free_files(&test);
}
