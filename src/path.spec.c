/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "path.h"

Test(get_pathname, simple) {
	char *pathname = get_pathname("foo", "bar");
	cr_expect_str_eq(pathname, "foo/bar");
	free(pathname);
}

Test(get_pathname, from_root) {
	char *pathname = get_pathname("/", "foo");
	cr_expect_str_eq(pathname, "/foo");
	free(pathname);
}

Test(basename, norm) {
	cr_expect_str_eq(get_basename("foo/bar"), "bar");
	cr_expect_str_eq(get_basename("/foo"), "foo");
	cr_expect_str_eq(get_basename("foo"), "foo");
	cr_expect_str_eq(get_basename("foo/bar/baz"), "baz");
}
