/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <ft_printf.h>
#include <stdlib.h>
#include "libft.h"
#include "conversions.h"
#include "opt.h"

static char	get_file_type(mode_t mode)
{
	if (S_ISDIR(mode))
		return ('d');
	else if (S_ISLNK(mode))
		return ('l');
	else if (S_ISBLK(mode))
		return ('b');
	else if (S_ISCHR(mode))
		return ('c');
	else if (S_ISSOCK(mode))
		return ('s');
	else if (S_ISFIFO(mode))
		return ('p');
	else
		return ('-');
}

void		mode_conversion(t_modestr perm, mode_t mode)
{
	perm[0] = get_file_type(mode);
	perm[1] = mode & S_IRUSR ? 'r' : '-';
	perm[2] = mode & S_IWUSR ? 'w' : '-';
	if (mode & S_ISUID)
		perm[3] = mode & S_IXUSR ? 's' : 'S';
	else
		perm[3] = mode & S_IXUSR ? 'x' : '-';
	perm[4] = mode & S_IRGRP ? 'r' : '-';
	perm[5] = mode & S_IWGRP ? 'w' : '-';
	if (mode & S_ISGID)
		perm[6] = mode & S_IXGRP ? 's' : 'S';
	else
		perm[6] = mode & S_IXGRP ? 'x' : '-';
	perm[7] = mode & S_IROTH ? 'r' : '-';
	perm[8] = mode & S_IWOTH ? 'w' : '-';
	if (mode & S_ISVTX)
		perm[9] = mode & S_IXOTH ? 't' : 'T';
	else
		perm[9] = mode & S_IXOTH ? 'x' : '-';
	perm[10] = '\0';
	perm[11] = '\0';
}

static void	time_conversion_free(char ***split)
{
	int i;

	i = 0;
	while ((*split)[i] != NULL)
	{
		ft_strdel(&(*split)[i]);
		i++;
	}
	ft_memdel((void **)split);
}

int			time_conversion(char *string, struct s_fs_file *files, t_opt opt)
{
	time_t	current;
	time_t	pre_time;
	char	**split;

	pre_time = opt & OPT_SORT_ATIME ? files->atime : files->mtime;
	time(&current);
	split = ft_strsplit(ctime(&pre_time), ' ');
	if (!split)
		return (-1);
	split[CON_YEAR][4] = '\0';
	if (opt & OPT_LONG_TIME)
		ft_sprintf(string, "%s %2s %s %s", split[CON_MONTH_DAY],
					split[CON_MONTH], split[CON_TIME], split[CON_YEAR]);
	else if (current < pre_time || pre_time < current - (3600 * 24 * 30.5 * 6))
		ft_sprintf(string, "%s %2s  %s", split[CON_MONTH_DAY], split[CON_MONTH],
					split[CON_YEAR]);
	else
	{
		split[CON_TIME][5] = '\0';
		ft_sprintf(string, "%s %2s %s", split[CON_MONTH_DAY], split[CON_MONTH],
					split[CON_TIME]);
	}
	time_conversion_free(&split);
	return (0);
}
