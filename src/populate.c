/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <sys/xattr.h>
#include <sys/acl.h>
#include <grp.h>
#include <pwd.h>
#include <sys/types.h>
#include <ft_printf.h>
#include "fs.h"
#include "libft.h"
#include "opt.h"

void		populate_group_user(struct s_fs_file *files, t_opt opt)
{
	struct group		*gid;
	struct passwd		*uid;

	gid = getgrgid(files->gid);
	uid = getpwuid(files->uid);
	if (!(gid) || (!ft_strcmp(gid->gr_name, "_unknown")) || opt & OPT_ID_NUM)
		ft_snprintf(files->gid_name, sizeof(files->gid_name),
					"%d", (int)files->gid);
	else
		ft_snprintf(files->gid_name, sizeof(files->gid_name),
					"%s", gid->gr_name);
	if (!(uid) || (!ft_strcmp(uid->pw_name, "_unknown")) || opt & OPT_ID_NUM)
		ft_snprintf(files->uid_name, sizeof(files->uid_name),
					"%d", (int)files->uid);
	else
		ft_snprintf(files->uid_name, sizeof(files->uid_name),
					"%s", uid->pw_name);
}
