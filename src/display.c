/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>
#include "path.h"
#include "error.h"
#include "opt.h"
#include "fs.h"
#include "sort.h"
#include "filter.h"
#include "long_display.h"
#include "display.h"

static void	short_display(struct s_fs_file *files, int standalone)
{
	if (files == NULL)
		return ;
	if (standalone)
		ft_printf("%s\n", files->pathname);
	else
		ft_printf("%s\n", get_basename(files->pathname));
	return (short_display(files->next, standalone));
}

static void	print_seperator(void)
{
	static int	is_not_first;

	if (is_not_first)
		ft_printf("\n");
	is_not_first = 1;
}

void		display_standalone(t_opt options, struct s_fs_file *files)
{
	print_seperator();
	if (options & OPT_LONG)
		long_display(files, 1, options);
	else
		short_display(files, 1);
}

void		display(t_opt options, struct s_fs_file *files, char *path)
{
	print_seperator();
	if (path != NULL)
		ft_printf("%s:\n", path);
	if (options & OPT_LONG)
		long_display(files, 0, options);
	else
		short_display(files, 0);
}

void		display_dir_contents(t_opt options, int print_dir_name,
				struct s_fs_file *dirs)
{
	struct s_fs_file *dir_contents;

	if (dirs == NULL)
		return ;
	if (fs_read_dir(&dir_contents, dirs->pathname, options) == -1)
	{
		recovered_error(dirs->pathname);
		return (display_dir_contents(options, print_dir_name, dirs->next));
	}
	if (!(options & OPT_ALL))
		filter(&dir_contents, filter_out_hidden_files);
	merge_sort_list(&dir_contents, get_sort_func(options));
	if (options & OPT_REVERSE)
		dir_contents = fs_reverse(dir_contents);
	display(options, dir_contents, print_dir_name ? dirs->pathname : NULL);
	if (options & OPT_RECURSIVE)
	{
		filter(&dir_contents, filter_directories);
		display_dir_contents(options, print_dir_name, dir_contents);
	}
	fs_free_files(&dir_contents);
	return (display_dir_contents(options, print_dir_name, dirs->next));
}
