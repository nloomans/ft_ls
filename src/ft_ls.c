/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>
#include <libft.h>
#include <string.h>
#include <errno.h>
#include "global.h"
#include "error.h"
#include "fs.h"
#include "operands.h"
#include "array.h"
#include "opt.h"
#include "long_display.h"
#include "libft.h"
#include "display.h"
#include "sort.h"

int		ft_ls(t_opt options, struct s_str_array operands)
{
	struct s_fs_file	*resolved_operands;
	struct s_fs_file	*files;
	struct s_fs_file	*dirs;

	g_exit_code = 0;
	resolved_operands = NULL;
	files = NULL;
	dirs = NULL;
	if (operands.len == 0)
		resolve_operands(&resolved_operands, (char *[]){".", NULL}, options);
	else
		resolve_operands(&resolved_operands, operands.array, options);
	merge_sort_list(&resolved_operands, get_sort_func(options));
	if (options & OPT_REVERSE)
		resolved_operands = fs_reverse(resolved_operands);
	split_operands(&files, &dirs, options, &resolved_operands);
	if (files != NULL)
		display_standalone(options, files);
	display_dir_contents(options,
		options & OPT_RECURSIVE || operands.len > 1,
		dirs);
	fs_free_files(&files);
	fs_free_files(&dirs);
	return (g_exit_code);
}
