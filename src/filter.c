/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <libft.h>
#include <ft_printf.h>
#include <sys/stat.h>
#include "fs.h"
#include "opt.h"
#include "path.h"
#include "filter.h"

int		filter_out_hidden_files(struct s_fs_file *file)
{
	return (get_basename(file->pathname)[0] != '.');
}

int		filter_directories(struct s_fs_file *file)
{
	return (S_ISDIR(file->mode) &&
		ft_strcmp(get_basename(file->pathname), ".") != 0 &&
		ft_strcmp(get_basename(file->pathname), "..") != 0);
}

void	filter(struct s_fs_file **head,
			int filter_func(struct s_fs_file *file))
{
	struct s_fs_file	*next;

	if (*head == NULL)
		return ;
	if (filter_func(*head))
		return (filter(&(*head)->next, filter_func));
	next = (*head)->next;
	ft_memdel((void **)head);
	*head = next;
	return (filter(head, filter_func));
}
