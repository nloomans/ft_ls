/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <sys/stat.h>
#include <time.h>
#include "cr_stdout.spec.h"
#include "fs.h"
#include "display.h"

struct s_fs_file head = (struct s_fs_file){
	.pathname = "foo/bar",
	.mode = S_IFREG | 0644,
	.nlink = 60,
	.uid = 0,
	.uid_name = "root",
	.gid = 0,
	.gid_name = "root",
	.size = 42,
	.atime = 0,
	.mtime = 0,
	.blocks = 0	,
	.lnk_mode = 0,
	.lnk_pathname = "",
	.next = &(struct s_fs_file){
		.pathname = "foo/bar/baz",
		.mode = S_IFREG | 0644,
		.nlink = 60,
		.uid = 0,
		.uid_name = "root",
		.gid = 0,
		.gid_name = "root",
		.size = 42,
		.atime = 0,
		.mtime = 0,
		.blocks = 0,
		.lnk_mode = 0,
		.lnk_pathname = "",
		.next = &(struct s_fs_file){
			.pathname = "42",
			.mode = S_IFREG | 0755,
			.nlink = 60,
			.uid = 0,
			.uid_name = "root",
			.gid = 0,
			.gid_name = "root",
			.size = 42,
			.atime = 0,
			.mtime = 0,
			.blocks = 0,
			.lnk_mode = 0,
			.lnk_pathname = "",
		}
	}
};

Test(display_standalone, prints_full_pathname) {
	cr_stdout_init();
	display_standalone(0, &head);
	cr_stdout_eq(
		"foo/bar\n"
		"foo/bar/baz\n"
		"42\n");
}

Test(display, prints_basename) {
	cr_stdout_init();
	display(0, &head, NULL);
	cr_stdout_eq(
		"bar\n"
		"baz\n"
		"42\n");
}

Test(display, prints_header) {
	cr_stdout_init();
	display(0, &head, "src");
	cr_stdout_eq(
		"src:\n"
		"bar\n"
		"baz\n"
		"42\n");
}

Test(display, prints_seperators)
{
	cr_stdout_init();
	display(0, &head, "src");
	display(0, &head, "inc");
	cr_stdout_eq(
		"src:\n"
		"bar\n"
		"baz\n"
		"42\n"
		"\n"
		"inc:\n"
		"bar\n"
		"baz\n"
		"42\n");
}

Test(display_mixed, prints_seperators)
{
	cr_stdout_init();
	display_standalone(0, &head);
	display(0, &head, "src");
	display(0, &head, "inc");
	cr_stdout_eq(
		"foo/bar\n"
		"foo/bar/baz\n"
		"42\n"
		"\n"
		"src:\n"
		"bar\n"
		"baz\n"
		"42\n"
		"\n"
		"inc:\n"
		"bar\n"
		"baz\n"
		"42\n");
}

Test(display, long) {
	setenv("TZ", "UTC0", 1);
	tzset();

	cr_stdout_init();
	display(OPT_LONG, &head, "src");
	cr_stdout_eq(
		"src:\n"
		"total 0\n"
		"-rw-r--r-- 60 root root 42 Jan  1  1970 bar\n"
		"-rw-r--r-- 60 root root 42 Jan  1  1970 baz\n"
		"-rwxr-xr-x 60 root root 42 Jan  1  1970 42\n"
	);
}

Test(display_standalone, long) {
	setenv("TZ", "UTC0", 1);
	tzset();

	cr_stdout_init();
	display_standalone(OPT_LONG, &head);
	cr_stdout_eq(
		"-rw-r--r-- 60 root root 42 Jan  1  1970 foo/bar\n"
		"-rw-r--r-- 60 root root 42 Jan  1  1970 foo/bar/baz\n"
		"-rwxr-xr-x 60 root root 42 Jan  1  1970 42\n"
		);
}
