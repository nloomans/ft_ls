/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include <errno.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <libft.h>
#include <ft_printf.h>
#include "fs.h"
#include "path.h"
#include "populate.h"
#include "opt.h"

int			fs_read_file(struct s_fs_file *file, const char *pathname,
							t_opt opt)
{
	struct stat		statbuf;

	if (lstat(pathname, &statbuf) == -1)
		return (-1);
	ft_strncpy(file->pathname, pathname, PATH_MAX);
	file->mode = statbuf.st_mode;
	file->nlink = statbuf.st_nlink;
	file->uid = statbuf.st_uid;
	file->gid = statbuf.st_gid;
	file->size = statbuf.st_size;
	file->atime = statbuf.st_atime;
	file->mtime = statbuf.st_mtime;
	file->blocks = statbuf.st_blocks;
	file->rdev = statbuf.st_rdev;
	populate_group_user(file, opt);
	if (S_ISLNK(file->mode))
	{
		if (stat(pathname, &statbuf) == -1)
			file->lnk_mode = S_IFREG;
		else
			file->lnk_mode = statbuf.st_mode;
		if (readlink(pathname, file->lnk_pathname, PATH_MAX) == -1)
			return (-1);
	}
	return (0);
}

static int	read_files(struct s_fs_file **head, const char *path, DIR *dir,
						t_opt opt)
{
	struct dirent	*entry;
	char			*pathname;

	errno = 0;
	entry = readdir(dir);
	if (entry == NULL)
		return (errno ? -1 : 0);
	*head = (struct s_fs_file *)ft_memalloc(sizeof(**head));
	if (head == NULL)
		return (-1);
	pathname = get_pathname(path, entry->d_name);
	if (pathname == NULL)
	{
		ft_memdel((void **)&head);
		return (-1);
	}
	fs_read_file(*head, pathname, opt);
	ft_strdel(&pathname);
	return (read_files(&(*head)->next, path, dir, opt));
}

int			fs_read_dir(struct s_fs_file **files, const char *path, t_opt opt)
{
	DIR *dir;

	dir = opendir(path);
	if (dir == NULL)
		return (-1);
	if (read_files(files, path, dir, opt) == -1)
		return (-1);
	if (closedir(dir) == -1)
		return (-1);
	return (0);
}

void		fs_print_files(struct s_fs_file *head)
{
	if (head == NULL)
	{
		ft_dprintf(2, "(null-file)\n");
		return ;
	}
	ft_dprintf(2, "name:  %s\n", head->pathname);
	ft_dprintf(2, "mode:  %o\n", head->mode);
	ft_dprintf(2, "nlink: %lu\n", head->nlink);
	ft_dprintf(2, "uid:   %u\n", head->uid);
	ft_dprintf(2, "gid:   %u\n", head->gid);
	ft_dprintf(2, "size:  %ld\n", head->size);
	ft_dprintf(2, "atime: %ld\n", head->atime);
	ft_dprintf(2, "mtime: %ld\n", head->mtime);
	if (head->lnk_pathname[0] != '\0')
	{
		ft_dprintf(2, "link:  pathname: %s\n", head->lnk_pathname);
		ft_dprintf(2, "link:  mode:     %o\n", head->lnk_mode);
	}
	if (head->next)
	{
		ft_dprintf(2, "\n");
		fs_print_files(head->next);
	}
}

void		fs_free_files(struct s_fs_file **head)
{
	if (*head == NULL)
		return ;
	fs_free_files(&(*head)->next);
	ft_memdel((void **)head);
}
