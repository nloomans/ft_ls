/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>
#include <libft.h>
#include "global.h"
#include "array.h"
#include "opt.h"
#include "ft_ls.h"

int		main(int argc, char **argv)
{
	t_opt				options;
	struct s_str_array	operands;
	int					ret;

	g_argv0 = argv[0];
	options = parse_opt(argc - 1, argv + 1);
	operands = parse_operands(argc - 1, argv + 1);
	ret = ft_ls(options, operands);
	ft_memdel((void *)&operands.array);
	return (ret);
}
