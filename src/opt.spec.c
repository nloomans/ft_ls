/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <unistd.h>
#include <ft_printf.h>
#include "global.h"
#include "array.h"
#include "opt.h"

Test(parese_opt, normal) {
	cr_expect_eq(parse_opt(2, (char *[]){"-l", "-R"}),
		OPT_LONG | OPT_RECURSIVE);
	cr_expect_eq(parse_opt(1, (char *[]){"-lR"}),
		OPT_LONG | OPT_RECURSIVE);
	cr_expect_eq(parse_opt(3, (char *[]){"-l", "rt", "-R"}),
		OPT_LONG | OPT_RECURSIVE);
	cr_expect_eq(parse_opt(3, (char *[]){"-l", "", "-R"}),
		OPT_LONG | OPT_RECURSIVE);
	cr_expect_eq(parse_opt(2, (char *[]){"-R", "-R"}),
		OPT_RECURSIVE);
	cr_expect_eq(parse_opt(1, (char *[]){"-RR"}),
		OPT_RECURSIVE);
	cr_expect_eq(parse_opt(1, (char *[]){"-lRart"}),
		OPT_LONG | OPT_RECURSIVE | OPT_ALL | OPT_REVERSE | OPT_SORT_TIME);
	cr_expect_eq(parse_opt(0, (char *[]){}),
		0);
	cr_expect_eq(parse_opt(0, (char *[]){"-lRart"}),
		0);
	cr_expect_eq(parse_opt(1, (char *[]){"-tS"}), OPT_SORT_SIZE);
	cr_expect_eq(parse_opt(1, (char *[]){"-St"}), OPT_SORT_TIME);
}

Test(parse_opt, exists_on_error, .exit_code = 1) {
	close(2);
	g_argv0 = "ls";
	parse_opt(1, (char *[]){"-x"});
}

Test(parse_operands, normal) {
	struct s_str_array operands;

	operands = parse_operands(1, (char *[]){"hello"});
	cr_assert_eq(operands.len, 1);
	cr_assert_str_eq(operands.array[0], "hello");
	cr_assert_eq(operands.array[1], NULL);
	free(operands.array);

	operands = parse_operands(3, (char *[]){"hello", "-la", "there"});
	cr_assert_eq(operands.len, 2);
	cr_assert_str_eq(operands.array[0], "hello");
	cr_assert_str_eq(operands.array[1], "there");
	cr_assert_eq(operands.array[2], NULL);
	free(operands.array);

	operands = parse_operands(1, (char *[]){"-la"});
	cr_assert_eq(operands.len, 0);
	cr_assert_eq(operands.array[0], NULL);
	free(operands.array);

	operands = parse_operands(0, (char *[]){});
	cr_assert_eq(operands.len, 0);
	cr_assert_eq(operands.array[0], NULL);
	free(operands.array);
}
