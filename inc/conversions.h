/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONVERSIONS_H
# define CONVERSIONS_H

# define CON_MONTH_DAY 1
# define CON_MONTH 2
# define CON_TIME 3
# define CON_YEAR 4
# include "opt.h"
# include "fs.h"

typedef char	t_modestr[12];

void	mode_conversion(char *perm, mode_t mode);
int		time_conversion(char *string, struct s_fs_file *files, t_opt opt);

#endif
