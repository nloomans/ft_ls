/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef SORT_H
# define SORT_H

# include "fs.h"
# include "opt.h"

typedef int			t_sort_func(struct s_fs_file *a, struct s_fs_file *b);
t_sort_func			*get_sort_func(t_opt options);

int					sort_mtime(struct s_fs_file *a, struct s_fs_file *b);
int					sort_atime(struct s_fs_file *a, struct s_fs_file *b);
int					sort_name(struct s_fs_file *a, struct s_fs_file *b);
int					sort_size(struct s_fs_file *a, struct s_fs_file *b);
struct s_fs_file	*fs_reverse(struct s_fs_file *head);
void				merge_sort_list(struct s_fs_file **files,
					int sort_function(struct s_fs_file*, struct s_fs_file*));
#endif
