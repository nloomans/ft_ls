/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef FS_H
# define FS_H
# include <sys/types.h>
# include <limits.h>
# include "opt.h"

struct		s_fs_file
{
	struct s_fs_file	*next;
	char				pathname[PATH_MAX + 1];
	mode_t				mode;
	nlink_t				nlink;
	uid_t				uid;
	char				uid_name[32 + 1];
	gid_t				gid;
	char				gid_name[32 + 1];
	off_t				size;
	time_t				atime;
	time_t				mtime;
	blkcnt_t			blocks;
	dev_t				rdev;
	mode_t				lnk_mode;
	char				lnk_pathname[PATH_MAX + 1];
};

int			fs_read_file(struct s_fs_file *file, const char *pathname,
							t_opt opt);
int			fs_read_dir(struct s_fs_file **files, const char *path, t_opt opt);
void		fs_print_files(struct s_fs_file *head);
void		fs_free_files(struct s_fs_file **head);

#endif
