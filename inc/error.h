/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef ERROR_H
# define ERROR_H

/*
** The arguments provided by the user are invalid.
*/

# define ERROR_USER 1

/*
** A fatal error has happend.
**
** example: failed to malloc before starting the directory listing.
*/

# define ERROR_FATAL 2

/*
** A non-fatal error has happend
**
** example: failed to list a specific directory.
*/

# define ERROR_RECOVERED 3

void	recovered_error(char *path);
void	invalid_arg_error(char *arg);

#endif
