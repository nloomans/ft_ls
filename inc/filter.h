/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILTER_H
# define FILTER_H

# include "opt.h"
# include "fs.h"

int		filter_out_hidden_files(struct s_fs_file *file);
int		filter_directories(struct s_fs_file *file);
void	filter(struct s_fs_file **head,
			int filter_func(struct s_fs_file *file));

#endif
