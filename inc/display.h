/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef DISPLAY_H
# define DISPLAY_H

# include "opt.h"
# include "fs.h"

void	display_standalone(t_opt options, struct s_fs_file *files);
void	display(t_opt options, struct s_fs_file *files, char *path);
void	display_dir_contents(t_opt options, int print_dir_name,
			struct s_fs_file *dirs);

#endif
