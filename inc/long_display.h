/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef LONG_DISPLAY_H
# define LONG_DISPLAY_H

# include "fs.h"
# include "opt.h"
# include "populate.h"

struct	s_width
{
	size_t		uid;
	size_t		gid;
	size_t		size;
	size_t		link;
	size_t		major;
	size_t		minor;
};

void	long_display(struct s_fs_file *files, int print_total, t_opt options);

#endif
