/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef OPT_H
# define OPT_H

typedef unsigned long	t_opt;
# define OPT_LONG		(1 << 1)
# define OPT_RECURSIVE	(1 << 2)
# define OPT_ALL		(1 << 3)
# define OPT_REVERSE	(1 << 4)
# define OPT_SORT_TIME	(1 << 5)
# define OPT_SORT_SIZE	(1 << 6)
# define OPT_SORT_ATIME (1 << 7)
# define OPT_ID_NUM		(1 << 8)
# define OPT_OMIT_GROUP (1 << 9)
# define OPT_LONG_TIME	(1 << 10)

t_opt					parse_opt(int argc, char **argv);
struct s_str_array		parse_operands(int argc, char **argv);

#endif
