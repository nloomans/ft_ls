/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_ls                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef OPERANDS_H
# define OPERANDS_H

# include <limits.h>
# include <sys/stat.h>
# include "fs.h"
# include "opt.h"

void	resolve_operands(struct s_fs_file **files, char **operands, t_opt opt);
void	split_operands(struct s_fs_file **files, struct s_fs_file **dirs,
			t_opt opt, struct s_fs_file **operand);

#endif
