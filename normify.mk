# **************************************************************************** #
#                                                                              #
#                                                         ::::::::             #
#    ft_ls                                              :+:    :+:             #
#                                                      +:+                     #
#    By: dkroeke <dkroeke@student.codam.nl>           +#+                      #
#        nloomans <nloomans@student.codam.nl>        +#+                       #
#                                                   #+#    #+#                 #
#    License: MIT                                  ########   odam.nl          #
#                                                                              #
# **************************************************************************** #

include definitions.mk

ifneq ($(CFLAGS),)
$(call notice,"cflags: $(CFLAGS)")
endif

NAME=ft_ls

all: $(NAME)

# Libaries #--------------------------------------------------------------------

LIBFT_NAME=		ft
LIBFT_DIR=		libft
LIBFT_A=		$(LIBFT_DIR)/lib$(LIBFT_NAME).a
LIBFT_IFLAGS=	-I $(LIBFT_DIR)
LIBFT_LFLAGS=	-L $(LIBFT_DIR) -l$(LIBFT_NAME)

$(LIBFT_A):
	@printf "$(BLUE)MAKE$(RESET)\t%s\n" $(LIBFT_DIR)

	@$(MAKE) -C $(LIBFT_DIR)

FTPRINTF_NAME=		ftprintf
FTPRINTF_DIR=		ft_printf
FTPRINTF_A=			$(FTPRINTF_DIR)/lib$(FTPRINTF_NAME).a
FTPRINTF_IFLAGS=	-I $(FTPRINTF_DIR)
FTPRINTF_LFLAGS=	-L $(FTPRINTF_DIR) -l$(FTPRINTF_NAME)

$(FTPRINTF_A):
	@printf "$(BLUE)MAKE$(RESET)\t%s\n" $(FTPRINTF_DIR)

	@$(MAKE) -C $(FTPRINTF_DIR)

# Definitions #-----------------------------------------------------------------

SRC_DIR=		src
INC_DIR=		inc
OBJ_DIR=		obj

SRC_FILES=		$(wildcard $(SRC_DIR)/*.c)
INC_FILES=		$(wildcard $(INC_DIR)/*.h)
OBJ_FILES=		$(patsubst $(SRC_DIR)/%.c,$(OBJ_DIR)/%.o,$(SRC_FILES))

CFLAGS=			-Werror -Wall -Wextra
IFLAGS=			-I $(INC_DIR) $(LIBFT_IFLAGS) $(FTPRINTF_IFLAGS)
LFLAGS=			$(LIBFT_LFLAGS) $(FTPRINTF_LFLAGS)

# Regular rules #---------------------------------------------------------------

ft_ls: $(OBJ_FILES) $(LIBFT_A) $(FTPRINTF_A)
	@printf "$(GREEN)LINK$(RESET)\t%s\n" $@

	@$(CC) -o $@ $(OBJ_FILES) $(CFLAGS) $(LFLAGS)

$(OBJ_DIR):
	@printf "$(GREEN)MKDIR$(RESET)\t%s\n" $@

	@mkdir -p $@

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c $(INC_FILES) | $(OBJ_DIR)
	@printf "$(GREEN)CC$(RESET)\t%s\n" $@

	@$(CC) -o $@ -c $< $(CFLAGS) $(IFLAGS)

# Phonies #---------------------------------------------------------------------

clean:
	@$(MAKE) -C $(LIBFT_DIR) fclean
	@$(MAKE) -C $(FTPRINTF_DIR) fclean
	@rm -rf $(OBJ_DIR)

fclean: clean
	@$(MAKE) -C $(LIBFT_DIR) fclean
	@$(MAKE) -C $(FTPRINTF_DIR) fclean
	@rm -f $(NAME)

re:
	@$(MAKE) fclean
	@$(MAKE)

# Special #---------------------------------------------------------------------

.SECONDARY: $(OBJ_FILES)
.PHONY: all clean fclean re
